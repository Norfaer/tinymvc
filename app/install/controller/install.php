<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class ControllerInstall extends ControllerEnvokable {
    public function index() {
        $this->load->language('install');
        $this->load->model('install');
        $this->document = HtmlDoc::getInstance('layout.tpl');
        $this->document->setTitle($this->language['title']);
        $settings = $this->model_install->getStage1();
        $data=[];
        $data['header_1'] = $this->language['header_1'];
        $data['section_1'] = [];
        $data['section_1']['rows'] = [];
        $data['section_1']['title'] = $this->language['section_1'];
        $data['section_1']['headers'] = [$this->language['table_h1'], $this->language['table_h2'], $this->language['table_h3'], $this->language['table_h4']];
        $data['section_1']['rows'][] = [$this->language['table_1_r1'], $settings['phpver'], '5.5+' , $settings['phpver_valid'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_1']['rows'][] = [$this->language['table_1_r2'], $settings['fupload'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_on'] , $settings['fupload'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_1']['rows'][] = [$this->language['table_1_r3'], $settings['sauto'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_off'] , $settings['sauto'] ? $this->language['status_err'] : $this->language['status_ok']];
        $data['section_2'] = [];
        $data['section_2']['rows'] = [];
        $data['section_2']['title'] = $this->language['section_2'];
        $data['section_2']['headers'] = [$this->language['table_h5'], $this->language['table_h2'], $this->language['table_h3'], $this->language['table_h4']];
        $data['section_2']['rows'][] = [$this->language['table_2_r1'], $settings['ext_db'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_on'] , $settings['ext_db'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_2']['rows'][] = [$this->language['table_2_r2'], $settings['ext_gd'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_on'] , $settings['ext_gd'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_2']['rows'][] = [$this->language['table_2_r3'], $settings['ext_zlib'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_on'] , $settings['ext_zlib'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_2']['rows'][] = [$this->language['table_2_r4'], $settings['ext_zip'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_on'] , $settings['ext_zip'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_2']['rows'][] = [$this->language['table_2_r5'], $settings['ext_mcrypt'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_on'] , $settings['ext_mcrypt'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_2']['rows'][] = [$this->language['table_2_r6'], $settings['ext_curl'] ? $this->language['status_on'] : $this->language['status_off'], $this->language['status_on'] , $settings['ext_curl'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_3'] = [];
        $data['section_3']['rows'] = [];
        $data['section_3']['title'] = $this->language['section_3'];
        $data['section_3']['headers'] = [$this->language['table_h6'], $this->language['table_h7'], $this->language['table_h4']];
        $data['section_3']['rows'][] = [DOC_ROOT . $this->language['table_3_r1'], $settings['perm_dir'] , $settings['wri_dir'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_3']['rows'][] = [DOC_ROOT . $this->language['table_3_r2'], $settings['perm_file1'], $settings['wri_file1'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['section_3']['rows'][] = [DOC_ROOT . $this->language['table_3_r3'], $settings['perm_file2'], $settings['wri_file2'] ? $this->language['status_ok'] : $this->language['status_err']];
        $data['text_next'] = $this->language['text_next'];
        $data['next_ref'] = $this->url->link('install/stage2');
        $data['valid'] = $settings['valid'];
        $this->session['valid_stage_1'] = $settings['valid'];
        $this->document->addNode(new HtmlNode('content','stage.tpl', $data));
        $this->document->publish();
    }

    public function stage2() {
        $data=[];
        $this->load->language('install');
        $this->load->model('install');
        $this->document = HtmlDoc::getInstance('layout.tpl');
        $this->document->setTitle($this->language['title']);
        $data['header_2'] = $this->language['header_2'];        
        $data['section_4'] = $this->language['section_4'];        
        $data['section_5'] = $this->language['section_5'];        
        $data['text_next'] = $this->language['text_next'];
        $data['text_prev'] = $this->language['text_prev'];
        $data['entry_host'] = $this->language['entry_host'];
        $data['entry_port'] = $this->language['entry_port'];
        $data['entry_dbname'] = $this->language['entry_dbname'];
        $data['entry_login'] = $this->language['entry_login'];
        $data['entry_pass'] = $this->language['entry_pass'];
        
        
        
        
        $data['next_ref'] = $this->url->link('install/stage3');
        $data['prev_ref'] = $this->url->link('install/index');
        $data['submit_action'] = $this->url->link('install/stage2');
        $data['valid'] = true;
        $this->document->addNode(new HtmlNode('content','stage2.tpl', $data));
        $this->document->publish();
    }
    
    public function getFileStatus($path) {
        
    }
}