<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class ModelInstall extends Model {
    public function getPerm($path) {
        $perms = fileperms($path);
        if (($perms & 0xC000) == 0xC000) {
            // Сокет
            $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
            // Символическая ссылка
            $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
            // Обычный
            $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
            // Специальный блок
            $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
            // Директория
            $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
            // Специальный символ
            $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
            // Поток FIFO
            $info = 'p';
        } else {
            // Неизвестный
            $info = 'u';
        }
        // Владелец
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ? (($perms & 0x0800) ? 's' : 'x' ) : (($perms & 0x0800) ? 'S' : '-'));
        // Группа
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ? (($perms & 0x0400) ? 's' : 'x' ) : (($perms & 0x0400) ? 'S' : '-'));
        // Мир
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ? (($perms & 0x0200) ? 't' : 'x' ) : (($perms & 0x0200) ? 'T' : '-'));
        return $info;
    }
    
    
    public function getStage1() {
        $data = [];
        $valid = true;
        $data['phpver'] = phpversion();
        if (version_compare($data['phpver'], '5.4')) {
            $data['phpver_valid'] = true;
        }
        else {
            $valid = false;
            $data['phpver_valid'] = false;
        }
        $data['fupload'] = ini_get('file_uploads');
        if (!$data['fupload']) {
            $valid = true;
        }
        $data['sauto'] = ini_get('session.auto_start');
        if ($data['sauto']) {
            $valid = false;
        }
        $extensions = array_map('strtolower', get_loaded_extensions());
        $data['ext_db'] = array_search('mysqli', $extensions) || key_exists('pdo_mysql', $extensions)  || key_exists('pdo_pgsql', $extensions) || key_exists('pgsql', $extensions)!==false;
        if (!$data['ext_db']) {
            $valid = false;
        }
        $data['ext_gd'] = array_search('gd', $extensions)!==false;
        if (!$data['ext_gd']) {
            $valid = false;
        }
        $data['ext_zlib'] = array_search('zlib', $extensions)!==false;
        if (!$data['ext_zlib']) {
            $valid = false;
        }
        $data['ext_zip'] = array_search('zip', $extensions)!==false;
        if (!$data['ext_zip']) {
            $valid = false;
        }
        $data['ext_mcrypt'] = array_search('mcrypt', $extensions)!==false;
        if (!$data['ext_mcrypt']) {
            $valid = false;
        }
        $data['ext_curl'] = array_search('curl', $extensions)!==false;
        if (!$data['ext_curl']) {
            $valid = false;
        }
        $data['perm_dir'] = $this->getPerm('dev');
        $data['perm_file1'] = $this->getPerm('app/site/config.php');
        $data['perm_file2'] = $this->getPerm('app/admin/config.php');
        $data['wri_dir'] = is_writable('dev');
        if (!$data['wri_dir']) {
            $valid = false;
        }
        $data['wri_file1'] = is_writable('app/site/config.php');
        if (!$data['wri_file1']) {
            $valid = false;
        }
        $data['wri_file2'] = is_writable('app/admin/config.php');
        if (!$data['wri_file2']) {
            $valid = false;
        }
        $data['valid'] = $valid;
        return $data;
    }
}