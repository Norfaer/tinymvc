<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <script src="/js/jquery-1.12.4.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container" style="padding-top:30px;">
    <? ::block('content') ?>
</div>
</body>
</html>