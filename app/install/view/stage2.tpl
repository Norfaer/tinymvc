<form role="form" class="form-horizontal" action="<?=$submit_action?>" method="post">
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title"><?=$header_2?></h3></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-9 col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title"><?=$section_4?></h3></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="db-login"><?=$entry_host?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="db-host" name="db-host">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="db-login"><?=$entry_port?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="db-port" name="db-port">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="db-login"><?=$entry_dbname?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="db-name" name="db-name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="db-login"><?=$entry_login?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="db-login" name="db-login">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd"><?=$entry_pass?></label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="db-pass" name="db-pass">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3 class="panel-title"><?=$section_5?></h3></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="db-login"><?=$entry_login?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="db-login" name="db-login">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd"><?=$entry_pass?></label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="db-pass" name="db-pass">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 hidden-xs">
                    <div class="well">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <ul class="pager">
                <li class="next <?=$valid ? '' : 'disabled'?>"><a href="<?=$next_ref?>"><?=$text_next?></a></li>
                <li class="previous"><a href="<?=$prev_ref?>"><?=$text_prev?></a></li>
            </ul>
        </div>
    </div>
</form>
<script>
$('.pager .disabled a').on('click', function(e) {
    e.preventDefault();
});
</script>
