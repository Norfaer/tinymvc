<div class="panel panel-default">
    <div class="panel-heading"><h3 class="panel-title"><?=$header_1?></h3></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-9 col-xs-12">
                <table class="table table-bordered">
                    <caption class="text-success">1.&nbsp;<?=$section_1['title']?></caption>
                    <thead>
                        <tr>
                            <?foreach($section_1['headers'] as $header):?>
                            <th class="info"><?=$header?></th>
                            <?/foreach?>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach($section_1['rows'] as $row):?>
                        <tr>
                            <?foreach($row as $val):?>
                            <td><?=$val?></td>
                            <?/foreach?>
                        </tr>
                        <?/foreach?>
                    </tbody>
                </table>           
                <table class="table table-bordered">
                    <caption class="text-success">2.&nbsp;<?=$section_2['title']?></caption>
                    <thead>
                        <tr>
                            <?foreach($section_2['headers'] as $header):?>
                            <th class="info"><?=$header?></th>
                            <?/foreach?>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach($section_2['rows'] as $row):?>
                        <tr>
                            <?foreach($row as $val):?>
                            <td><?=$val?></td>
                            <?/foreach?>
                        </tr>
                        <?/foreach?>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <caption class="text-success">3.&nbsp;<?=$section_3['title']?></caption>
                    <thead>
                        <tr>
                            <?foreach($section_3['headers'] as $header):?>
                            <th class="info"><?=$header?></th>
                            <?/foreach?>
                        </tr>
                    <tbody>
                        <?foreach($section_3['rows'] as $row):?>
                        <tr>
                            <?foreach($row as $val):?>
                            <td><?=$val?></td>
                            <?/foreach?>
                        </tr>
                        <?/foreach?>
                    </tbody>
                    </thead>
                </table>
            </div>
            <div class="col-sm-3 hidden-xs">
                <div class="well">
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <ul class="pager">
            <li class="next <?=$valid ? '' : 'disabled'?>"><a href="<?=$next_ref?>"><?=$text_next?></a></li>
        </ul>
    </div>
</div>
<script>
$('.pager .disabled a').on('click', function(e) {
    e.preventDefault();
});
</script>