<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

define('APPLICATION', 'install');
define('APP_VIEW', 'app/' . APPLICATION . '/view/');
define('HTTP_ROOT','http://'.$this->request->server['HTTP_HOST'] . '/');
