<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

$_['title'] = 'TinyMVC | Інсталяція';
$_['header_1']='Встановлення TinyMVC (Крок 1 з 2)';
$_['header_2']='Встановлення TinyMVC (Крок 2 з 2)';

$_['text_next']='Далі';
$_['text_prev']='Назад';

$_['status_on'] = 'Увімк.';
$_['status_off'] = 'Вимк.';
$_['status_ok'] = '<span class="glyphicon glyphicon-ok text-success"></span>';
$_['status_err'] = '<span class="glyphicon glyphicon-remove text-danger"></span>';

$_['section_1'] = 'Налаштуйте будьласка PHP згідно наступних вимог:';
$_['section_2'] = 'Встановіть та налаштуйте наступні розширення PHP:';
$_['section_3'] = 'Вказані файли та теки повині мати дозвіл на перезапис:';
$_['sextion_4'] = 'Налаштування бази даних:';
$_['sextion_5'] = 'Налаштування авторизації:';

$_['table_h1'] = 'Опція PHP';
$_['table_h2'] = 'Поточні налаштуання';
$_['table_h3'] = 'Вимоги';
$_['table_h4'] = 'Статус';
$_['table_h5'] = 'Розширення PHP';
$_['table_h6'] = 'Путь';
$_['table_h7'] = 'Дозвіли';


$_['table_1_r1'] = 'Версія PHP';
$_['table_1_r2'] = 'Завантаження файлів';
$_['table_1_r3'] = 'Автозапуск сесії';
$_['table_2_r1'] = 'Бази даних';
$_['table_2_r2'] = 'GD';
$_['table_2_r3'] = 'Zlib';
$_['table_2_r4'] = 'Zip';
$_['table_2_r5'] = 'mCrypt';
$_['table_2_r6'] = 'cUrl';
$_['table_3_r1'] = 'dev/';
$_['table_3_r2'] = 'app/site/config.php';
$_['table_3_r3'] = 'app/admin/config.php';

$_['entry_host'] = 'Хост';
$_['entry_port'] = 'Порт';
$_['entry_login'] = 'Логін';
$_['entry_pass'] = 'Пароль';
$_['entry_dbname'] = 'Им\'я бази';
