<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

$_['title'] = 'TinyMVC | Установка';
$_['header_1']='Установка TinyMVC (Шаг 1 из 2):';
$_['header_2']='Установка TinyMVC (Шаг 2 из 2):';

$_['text_next']='Далее';
$_['text_prev']='Назад';

$_['status_on'] = 'Вкл.';
$_['status_off'] = 'Выкл.';
$_['status_ok'] = '<span class="glyphicon glyphicon-ok text-success"></span>';
$_['status_err'] = '<span class="glyphicon glyphicon-remove text-danger"></span>';

$_['section_1'] = 'Настройте пожалуйста PHP для удовлетворения требованиям:';
$_['section_2'] = 'Установите и настройте следующие расширения PHP:';
$_['section_3'] = 'Следующие файлы и каталоги должны иметь разрешение на запись:';
$_['section_4'] = 'Настройка базы данных:';
$_['section_5'] = 'Настройка авторизации:';

$_['table_h1'] = 'Опция PHP';
$_['table_h2'] = 'Текущая настройка';
$_['table_h3'] = 'Требования';
$_['table_h4'] = 'Статус';
$_['table_h5'] = 'Расширение PHP';
$_['table_h6'] = 'Путь';
$_['table_h7'] = 'Разрешения';

$_['table_1_r1'] = 'Версия PHP';
$_['table_1_r2'] = 'Загрузка файлов';
$_['table_1_r3'] = 'Автозапуск сессии';

$_['table_2_r1'] = 'Базы данных';
$_['table_2_r2'] = 'GD';
$_['table_2_r3'] = 'Zlib';
$_['table_2_r4'] = 'Zip';
$_['table_2_r5'] = 'mCrypt';
$_['table_2_r6'] = 'cUrl';

$_['table_3_r1'] = 'dev/';
$_['table_3_r2'] = 'app/site/config.php';
$_['table_3_r3'] = 'app/admin/config.php';

$_['entry_host'] = 'Хост';
$_['entry_port'] = 'Порт';
$_['entry_login'] = 'Логин';
$_['entry_pass'] = 'Пароль';
$_['entry_dbname'] = 'Имя базы';
