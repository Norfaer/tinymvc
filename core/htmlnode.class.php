<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class HtmlNode {
    private $data;
    private $name;
    private $template;
    private $children = [];

    public function __construct($name, $template, &$data) {
        $this->data = &$data;
        $this->name = $name;
        $this->template = APP_VIEW . $template;
        if (!file_exists($this->template)) {
            throw new Exception('Не найден файл шаблона: ' . $this->template, 920);
        }
    }
    
    public function addNode(HtmlNode $node) {
        $this->children[$node->name] = $node;
    }
    
    public function removeNode(HtmlNode $node) { 
        unset($this->children[$node->name]);
    }
    
    public function publish() {
        extract($this->data);
        if (file_exists($this->template)) {
            include 'tpl://'.$this->template;
        }
    }

    public function updateData(&$data) {
        unset($this->data);
        $this->data = &$data;
    }
    
    public function publishNode($name) {
        if (isset($this->children[$name])) {
            $this->children[$name]->publish();
        } 
    }
}