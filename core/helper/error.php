<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

function error_handler($errno, $errstr, $errfile, $errline) {
    if (error_reporting() && $errno !=E_NOTICE) {
        throw new Exception($errstr, $errno);
    }
    else {
        return false;
    }
}

error_reporting(-1);
set_error_handler('error_handler');