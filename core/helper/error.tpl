<!DOCTYPE html>
<html>
<head>
    <title>Ошибка!</title>
    <style>
        body {
                font-size: 12px;
                font-family: arial, helvetica, sans-serif;
                color: #333;
        }
        #head {
                margin: 1em;
                padding: 0.5em 1em;
                background-color: #e3e3e3;
                border: 1px solid #ccc;
        }
        #mydiv {
                position:absolute;
                top: 50%;
                left: 50%;
                width:60em;
//                height:18em;
                margin-top: -9em; /*set to a negative number 1/2 of your height*/
                margin-left: -30em; /*set to a negative number 1/2 of your width*/
                border: 1px solid #ccc;
                background-color: #f3f3f3;
        }
        table,td,th {
                border: 1px solid #ccc;
        }
        table {
            margin: 1em;
            width: 58em;
            border-collapse: collapse;
            
        }
        th {
            padding: 0.5em 1em;
            background-color: #FFE88E;
        }
        td {
            padding: 0.5em 1em;
            background-color: #ffffff;
        }
    </style>
</head>
<body>
    <div id="mydiv">
        <div id="head">
            <h2>Ошибка во время исполнения: </h2>
            <h4>Ошибка № <?php echo($e->getCode()); ?></h4>
            <h4>Сообщение: <?php echo($e->getMessage()); ?></h4>
        </div>
            <table>
                <caption><b>Стек вызовов:</b></caption>
                <tr><th>Функция</th><th>Строка №</th><th>Файл</th><th>Класс</th><th>Тип</th></tr>
                <?php foreach($backtrace as $row) { ?>
                <tr>
                    <td><?php echo isset($row['function'])?$row['function']:''; ?></td>
                    <td><?php echo isset($row['line'])?$row['line']:''; ?></td>
                    <td><?php echo isset($row['file'])?$row['file']:''; ?></td>
                    <td><?php echo isset($row['class'])?$row['class']:''; ?></td>
                    <td><?php echo isset($row['type'])?$row['type']:''; ?></td>
                </tr>
                <?php } ?>
            </table>
    </div>
</body>
</html>