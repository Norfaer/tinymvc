<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

abstract class UrlWrapper {
    use Singleton;
    private $registry;

    abstract function link($route);
    abstract function unlink($link);
    
    public function init() {
        $this->registry = Registry::getInstance();
    }
    
    public function __get($key) {
            return $this->registry[$key];
    }

    public function __set($key, $value) {
            $this->registry[$key]=$value;
    }
}