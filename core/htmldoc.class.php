<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class HtmlDoc {
    use Singleton;
    private $registry;
    private $root;
    private $data=['styles' => [],'scripts' => [], 'title' => ''];
    
    public function init($layout) {
        stream_wrapper_register("tpl", "TplPipe");
        $this->registry = Registry::getInstance();
        $this->root = new HtmlNode('root', $layout, $this->data);
    }
    
    public function addScript($src) {
        $this->data['scripts'][] = $src;
    }
    
    public function addStyle($src) {
        $this->data['styles'][] = $src;
    }
    
    public function setTitle($title) {
        $this->data['title'] = $title;
    }
    
    public function addMeta($meta) {
        
    }
    
    public function addNode(HtmlNode $node) {
        $this->root->addNode($node);
    }
    
    public function removeNode(HtmlNode $node) {
        $this->root->removeNode($node);
    }

    public function publish() {
        $this->root->publish();
    }

    public function __get($key) {
            return $this->registry[$key];
    }

    public function __set($key, $value) {
            $this->registry[$key]=$value;
    }
    
    public function __isset($key) {
        return isset($this->registry[$key]);
    }
}