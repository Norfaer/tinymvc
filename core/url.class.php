<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class Url extends UrlWrapper {
    private $handler = null;
        
    public function link($route) {
        if ($this->handler) {
            return $this->handler->link($route);
        }
        else {
            return HTTP_ROOT . $this->locale . '/' .$this->application . '/' .$route;
        }
    }
    
    public function unlink($link) {
        if ($this->handler) {
            return $this->handler->unlink($link);
        }
        else {
            return ['application'=>'','controller'=>'','action'=>''];
        }
    }
    
    public function setHandler($object) {
        if (is_subclass_of($object, 'UrlWrapper')) {
            $this->handler = $object;
        }
        throw new Exception('Неверный обработчик для Url', 920);
    }    
}