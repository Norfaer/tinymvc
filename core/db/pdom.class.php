<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

namespace Db{

class PdoMysql extends \DbAdapter {
    private $statement;
    
    public function __construct($host, $login, $pass, $dbname, $port=3306) {
        $this->dbh = new \PDO('mysql:host=' . $host . ';port=' . $port . ';dbname=' . $dbname, $login, $pass, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,\PDO::ATTR_PERSISTENT => true));
        $this->dbh->exec("SET NAMES 'utf8'");
        $this->dbh->exec("SET CHARACTER SET utf8");
        $this->dbh->exec("SET CHARACTER_SET_CONNECTION=utf8");
        $this->dbh->exec("SET SQL_MODE = ''");
    }
    
    public function query($query) {
        $this->statement = $this->dbh->prepare($sql);
	$result = false;
        if ($this->statement && $this->statement->execute()) {
            $data = array();
            while ($row = $this->statement->fetch(\PDO::FETCH_ASSOC)) {
                $data[] = $row;
            }
            $result = new \stdClass();
            $result->row = (isset($data[0]) ? $data[0] : array());
            $result->rows = $data;
            $result->num_rows = $this->statement->rowCount();
        }
        if ($result) {
            return $result;
        } else {
            $result = new \stdClass();
            $result->row = array();
            $result->rows = array();
            $result->num_rows = 0;
            return $result;
        }
    }

    public function escape($str) {
        return str_replace(array("\\", "\0", "\n", "\r", "\x1a", "'", '"'), array("\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"'), $str);
    }

    public function getCountAffected() {
        if ($this->statement) {
                return $this->statement->rowCount();
        } else {
                return 0;
        }
    }

    public function getLastId() {
            return $this->pdo->lastInsertId();
    }

    public function __destruct() {
            $this->pdo = null;
    }    
} 
}