<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

namespace Db {

final class Mysqli extends \DbAdapter {
    public function __construct($host, $login, $pass, $dbname, $port = 3306) {
        $this->dbh = new \mysqli($host, $login, $pass, $dbname, $port);
        if ($this->dbh->connect_error) {
            throw new Exception($this->dbh->connect_errno,$this->dbh->connect_error);
        }
        $this->dbh->set_charset('utf8');
        $this->dbh->query("SET SQL_MODE = ''");
    }
    
    public function query($query) {
        $result = $this->dbh->query($query);
        if ($result === false ){
            throw new Exception($this->dbh->errno,$this->dbh->error);
        }
        if ($result instanceof \mysqli_result) {
            $data = array();
            while ($row = $result->fetch_assoc()) {
                    $data[] = $row;
            }
            $result->close();
            $ret = new \stdClass();
            $ret->num_rows = $result->num_rows;
            $ret->row = isset($data[0]) ? $data[0] : array();
            $ret->rows = &$data;
            return $ret;
        } else {
            return true;
        }
    }
    
    public function escape($str) {
        return $this->dbh->real_escape_string($str);
    }
    
    public function getLastId() {
        return $this->dbh->insert_id;
    }
    
    public function getCountAffected() {
        return $this->dbh->affected_rows;
    }
    
    public function __destruct() {
        $this->dbh->close();
        $this->dbh = null;
    }
}

}