<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

namespace Db;

final class Postgres {

    public function __construct($host, $login, $pass, $dbname, $port = '5432') {
        if (!$this->dbh = pg_connect('hostname=' . $host . ' port=' . $port .  ' username=' . $login . ' password='	. $pass . ' database=' . $dbname)) {
            throw new Exception('Error: Could not make a database link using ' . $login . '@' . $host);
        }
        pg_query($this->dbh, "SET CLIENT_ENCODING TO 'UTF8'");
    }

    public function query($sql) {
        $resource = pg_query($this->dbh, $sql);
        if ($resource === false) {
            throw new Exception('PostgreSQL error: '.  pg_last_error($this->dbh) );
        }
        if (is_resource($resource)) {
            $i = 0;
            $data = array();
            while ($result = pg_fetch_assoc($resource)) {
                $data[$i] = $result;
                $i++;
            }
            pg_free_result($resource);
            $query = new \stdClass();
            $query->row = isset($data[0]) ? $data[0] : array();
            $query->rows = &$data;
            $query->num_rows = $i;
            return $query;
        } else {
            return true;
        }
    }

    public function escape($str) {
        return pg_escape_string($this->dbh, $str);
    }

    public function countAffected() {
        return pg_affected_rows($this->dbh);
    }

    public function getLastId() {
        $query = $this->query("SELECT LASTVAL() AS `id`");
        return $query->row['id'];
    }

    public function __destruct() {
        pg_close($this->dbh);
        $this->dbh = null;
    }
}