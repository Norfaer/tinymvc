<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

interface Cache extends ArrayAccess {
    function set($key, $value, $expire);
    function get($key);
    function delete($key);
    function exists($key);
}