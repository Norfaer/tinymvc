<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

abstract class DbAdapter {
    protected $dbh;
    abstract public function query($query);
    abstract public function escape($str);
    abstract public function getLastId();
    abstract public function getCountAffected();
    abstract public function __construct($host, $login, $pass, $dbname, $port = false); 
    abstract public function __destruct(); 
}