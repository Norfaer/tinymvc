<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

namespace Cache;

final class Apc implements \Cache {
    private $prefix;
    private $expire;
    
    public function __construct($prefix = '', $expire = 3600) {
        $this->expire = $expire;
        $this->prefix = $prefix;
    }

    public function set($key, $value) {
        if ($key) {
            apc_store($this->prefix . $key, $value, $this->expire);
        }
    }
    
    public function get($key) {
        return apc_fetch($this->prefix . $key);
    }
    
    public function exists($key) {
        return apc_exists($this->prefix . $key);
    }
    
    public function delete($key) {
        apc_delete($this->prefix . $key);
    }
        
    public function offsetSet($offset, $value) {
        if ($offset) {
            apc_store($this->prefix . $offset, $value, $this->expire);
        } 
    }

    public function offsetExists($offset) {
        return apc_exists($this->prefix . $offset);
    }

    public function offsetUnset($offset) {
        apc_delete($this->prefix . $offset);
    }

    public function offsetGet($offset) {
        return apc_fetch($this->prefix . $offset);
    }
}
