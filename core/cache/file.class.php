<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

namespace Cache;

final class File implements \Cache {
    private $prefix;
    private $expire;
    
    public function __construct($prefix = '', $expire = 3600) {
        $this->expire = $expire;
        $this->prefix = $prefix;
    }

    public function set($key, $value) {
        if ($key) {
            $filename = 'dev/cache/' . $this->prefix . $key;
            $handle = fopen($filename, 'wb');
            flock($handle, LOCK_EX);
            fwrite($handle, json_encode($value, JSON_UNESCAPED_UNICODE));
            fflush($handle);
            flock($handle, LOCK_UN);
            fclose($handle);
        }
    }
    
    public function get($key) {
        $filename = 'dev/cache/' . $this->prefix . $key;
        if (file_exists($filename)) {
            $time = filemtime($filename);
            if ($time + $this->expire < time()) {
                return json_decode(file_get_contents($filename));
            } else {
                unlink($filename);
                return false;
            }
        }
        return false;
    }
    
    public function exists($key) {
        return file_exists('dev/cache/' . $this->prefix . $key);
    }
    
    public function delete($key) {
        $filename = 'dev/cache/' . $this->prefix . $key;
        if (file_exists($filename)) {
                unlink($filename);
        }        
    }
        
    public function offsetSet($offset, $value) {
        if ($offset) {
            $filename = 'dev/cache/' . $this->prefix . $offset;
            $handle = fopen($filename, 'wb');
            flock($handle, LOCK_EX);
            fwrite($handle, json_encode($value, JSON_UNESCAPED_UNICODE));
            fflush($handle);
            flock($handle, LOCK_UN);
            fclose($handle);
        } 
    }

    public function offsetExists($offset) {
        return file_exists('dev/cache/' . $this->prefix . $offset);
    }

    public function offsetUnset($offset) {
        $filename = 'dev/cache/' . $this->prefix . $offset;
        if (file_exists($filename)) {
                unlink($filename);
        }        
    }

    public function offsetGet($offset) {
        $filename = 'dev/cache/' . $this->prefix . $offset;
        if (file_exists($filename)) {
            $time = filemtime($filename);
            if ($time + $this->expire < time()) {
                return json_decode(file_get_contents($filename));
            } else {
                unlink($filename);
                return false;
            }
        }
        return false;
    }
}