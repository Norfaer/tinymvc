<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

namespace Cache;

final class Mem implements \Cache{
    private $expire;
    private $prefix;
    private $mem;
    
    public function __construct($host, $port, $prefix='', $expire=3600) {
        $this->expire = $expire;
        $this->prefix = $prefix;
        $this->mem = new \Memcache;
        $this->mem->pconnect($host, $port);
    }
    
    public function set($key, $value) {
        if ($key) {
            $this->mem->set($this->prefix .$key, $value, MEMCACHE_COMPRESSED, $this->expire);
        }
    }
    
    public function get($key) {
        return $this->mem->get($this->prefix . $key);
    }
    
    public function exists($key) {
        return false;
    }
    
    public function delete($key) {
        $this->mem->delete($this->prefix . $key);
    }
        
    public function offsetSet($offset, $value) {
        if ($offset) {
            $this->mem->set($this->prefix .$offset, $value, MEMCACHE_COMPRESSED, $this->expire);
        } 
    }

    public function offsetExists($offset) {
        return false;
    }

    public function offsetUnset($offset) {
        $this->mem->delete($this->prefix . $offset);
    }

    public function offsetGet($offset) {
        return $this->mem->get($this->prefix . $offset);
    }    
}