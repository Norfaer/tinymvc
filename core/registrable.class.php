<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

trait Registrable
{
    protected $registry;
    
    final public function __get($key) {
            return $this->registry[$key];
    }

    final public function __set($key, $value) {
            $this->registry[$key]=$value;
    }
}