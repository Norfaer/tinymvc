<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class Session implements ArrayAccess {
    use Singleton;
    
    public function init() {
        if (!session_id()) {
                ini_set('session.use_only_cookies', 'On');
                ini_set('session.use_cookies', 'On');
                ini_set('session.use_trans_sid', 'Off');
                ini_set('session.cookie_httponly', 'On');
                if (isset($_COOKIE[session_name()]) && !preg_match('/^[a-zA-Z0-9,\-]{22,40}$/', $_COOKIE[session_name()])) {
                        die('Fatal error');
                }
                session_set_cookie_params(0, '/');
                session_start();
        }
    }
    
    public function getId() {
        return session_id();
    }
    
    public function destroy() {
        session_destroy();
    }
    
    public function offsetSet($offset, $value) {
        if (!is_null($offset)) {
            $_SESSION[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($_SESSION[$offset]);
    }

    public function offsetUnset($offset) {
        unset($_SESSION[$offset]);
    }

    public function offsetGet($offset) {
        return isset($_SESSION[$offset]) ? $_SESSION[$offset] : null;
    }

    public function __get($key) {
            return $_SESSION[$key];
    }

    public function __set($key, $value) {
            $_SESSION[$key]=$value;
    }
    
    public function __isset($key) {
        return isset($_SESSION[$key]);
    }
    
    public function __unset($key) {
        unset($_SESSION[$key]);
    }
}