<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class Cookie implements ArrayAccess {
    private $path;
    private $expire;
    
    use Singleton;
    
    public function init() {
        $this->expire = 3600;
        $this->path = '/';
    }
    
    public function setTime($expire) {
        $this->expire = $expire;
    }
    
    public function setPath($path) {
        $this->path = $path;
    }
    
    public function offsetSet($offset, $value) {
        if (!is_null($offset)) {
            setcookie($offset, json_encode($value), time() +  $this->expire, $this->path);
        }
    }

    public function offsetExists($offset) {
        return isset($_COOKIE[$offset]);
    }

    public function offsetUnset($offset) {
        if (!is_null($offset)) {
            setcookie($offset, '', time()-3600, $this->path);
        }
    }

    public function offsetGet($offset) {
        return isset($_COOKIE[$offset]) ? json_decode($_COOKIE[$offset]) : null;
    }

    public function __get($key) {
        return isset($_COOKIE[$key]) ? json_decode($_COOKIE[$key]) : null;
    }

    public function __set($key, $value) {
        if (!is_null($key)) {
            setcookie($key, json_encode($value), time() +  $this->expire, $this->path);
        }
    }
    
    public function __isset($key) {
        return isset($_COOKIE[$key]);
    }
    
    public function __unset($key) {
        if (!is_null($key)) {
            setcookie($key, '', time() - 3600, $this->path);
        }
    }
}