<?php

/* 
 * This code is part of TinyMVC project
 * Author: Alexander Firsov  * 
 */

class TplPipe {
    private $pos;
    private $buf;
    private $filename;

    function stream_open($path, $mode, $options, &$opened_path) {
        $this->filename = substr($path,6);
        $this->buf = file_get_contents($this->filename);
        $this->buf = strtr($this->buf,['::block('=>'$this->publishNode(','/foreach'=>'endforeach;','/for'=>'endfor;','<?='=>'<?php echo ','/if'=>'endif;','<?'=>'<?php ']);
        $this->pos = 0;
        return true;
    }
    function stream_read($count) {
            $ret = substr($this->buf, $this->pos, $count);
            $this->pos += strlen($ret);
            return $ret;
    }
    function stream_eof() {
        return $this->pos >= strlen($this->buf);
    }
    function stream_stat() {
        return stat($this->filename);
    }
}

